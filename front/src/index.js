/* eslint-disable */
import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware, compose } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunkMiddleware from "redux-thunk";
import * as serviceWorker from "./serviceWorker";
import AppLayout from "./AppLayout";
import rootReducer from "./reducers";
import { todoAdded, toggledTodo, movedTodo, removedTodo, editedTodo } from "./reducers/actions";
import SocketIOHelper, { SocketActions } from "./SocketIoHelper";
import HttpWrapper from "./httpWrapper/wrapper";
import { basePath } from "./settings";

const store = createStore(rootReducer, compose(
  applyMiddleware(thunkMiddleware),
  composeWithDevTools(
    applyMiddleware(thunkMiddleware)
  )
)
);

SocketIOHelper.getSocket().on(SocketActions.ADD_TODO, (id: string, text: string) => {
  store.dispatch(todoAdded(id, text));
});

SocketIOHelper.getSocket().on(SocketActions.TOGGLE_TODO, (id: string, completed: boolean) => {
  store.dispatch(toggledTodo(id, completed));
});

SocketIOHelper.getSocket().on(SocketActions.MOVE_TODO, (id: string, index: number) => {
  store.dispatch(movedTodo(id, index));
});

SocketIOHelper.getSocket().on(SocketActions.REMOVE_TODO, (id: string) => {
  store.dispatch(removedTodo(id));
});

SocketIOHelper.getSocket().on(SocketActions.EDIT_TODO, (id: string, text: string) => {
  store.dispatch(editedTodo(id, text));
});

HttpWrapper.init(basePath);

ReactDOM.render(
  <Provider store={store}>
    <AppLayout />
  </Provider>, document.getElementById("root")
);

serviceWorker.unregister();
