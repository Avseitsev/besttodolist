// @flow
import type { ThunkAction } from "redux-thunk";
import {
  ADD_TODO,
  TODO_ADDED,
  SET_VISIBILITY_FILTER,
  TOGGLE_TODO,
  TOGGLED_TODO,
  GET_TODOS,
  RECEIVE_TODOS,
  MOVE_TODO,
  MOVED_TODO,
  REMOVE_TODO,
  EDIT_TODO,
  EDITED_TODO,
  REMOVED_TODO
} from "./actions";
import {
  TodoAddedAction,
  ToggledTodoAction,
  ReceiveTodosAction,
  MovedTodoAction,
  RemovedTodoAction,
  EditedTodoAction
} from "../todos";
import type { Todo, TodosAction } from "../todos";
import { SetVisibilityFilterAction } from "../filters";
import SocketIOHelper, { SocketActions } from "../../SocketIoHelper";
import {
  getTodos as getTodosRequest,
  addTodo as addTodoRequest,
  toggleTodo as toggleTodoRequest,
  moveTodo as moveTodoRequest,
  removeTodo as removeTodoRequest,
  editTodo as editTodoRequest
} from "../../httpWrapper/todoQuery";

const todoExtractor = (data: Object): Todo[] => {
  return data.map(respTodo => (
    {
      id: respTodo._id,
      text: respTodo.text,
      completed: respTodo.completed,
    }
  ));
};

export const receiveTodos = (todos: Todo[]): ReceiveTodosAction => ({
  type: RECEIVE_TODOS,
  todos
});

export const getTodos = (): ThunkAction<TodosAction> => (dispatch) => {

  dispatch({ type: GET_TODOS });

  getTodosRequest().then(data => {
    dispatch(receiveTodos(todoExtractor(data.data)));
  }).catch((error) => console.log(`error: getTodost -> ${error}`));
};

export const todoAdded = (id: string, text: string): TodoAddedAction => ({
  type: TODO_ADDED,
  id,
  text
});

export const addTodo = (text: string): ThunkAction<TodosAction> => (dispatch) => {

  dispatch({ type: ADD_TODO });

  addTodoRequest(text, false).then(data => {
    const id = data.data._id;
    SocketIOHelper.getSocket().emit(SocketActions.ADD_TODO, id, text);
    dispatch(todoAdded(id, text));
  }).catch((error) => console.log(`error: addTodoRequest -> ${error}`));
};

export const setVisibilityFilter = (filter: string): SetVisibilityFilterAction => ({
  type: SET_VISIBILITY_FILTER,
  filter
});

export const toggledTodo = (id: string, completed: boolean): ToggledTodoAction => ({
  type: TOGGLED_TODO,
  id,
  completed
});

export const toggleTodo = (id: string): ThunkAction<TodosAction> => (dispatch, getState) => {
  const completed = !getState().todos.todosData.find(x => x.id === id).completed;

  dispatch({ type: TOGGLE_TODO, id, completed });

  toggleTodoRequest(id, completed).then(() => {
    SocketIOHelper.getSocket().emit(SocketActions.TOGGLE_TODO, id, completed);
  }).catch((error) => {
    dispatch(toggledTodo(id, !completed));
    console.log(`error: toggleTodoRequest -> ${error}`);
  });
};

export const movedTodo = (id: string, index: number): MovedTodoAction => ({
  type: MOVED_TODO,
  id,
  index
});

export const moveTodo = (id: string, index: number): ThunkAction<TodosAction> => (dispatch, getState) => {
  const oldIndex = getState().todos.todosData.findIndex(x => x.id === id);

  dispatch({ type: MOVE_TODO, id, index });

  moveTodoRequest(id, index).then(() => {
    SocketIOHelper.getSocket().emit(SocketActions.MOVE_TODO, id, index);
  }).catch((error) => {
    dispatch(movedTodo(id, oldIndex));
    console.log(`error: move todo -> ${error}`);
  });
};

export const removedTodo = (id: string): RemovedTodoAction => ({
  type: REMOVED_TODO,
  id
});

export const removeTodo = (id: string): ThunkAction<TodosAction> => (dispatch) => {
  dispatch({ type: REMOVE_TODO, id });
  
  removeTodoRequest(id).then(() => {
    SocketIOHelper.getSocket().emit(SocketActions.REMOVE_TODO, id);
  }).catch((error) => {
    dispatch({ type: GET_TODOS });
    console.log(`error: remove todo -> ${error}`);
  });
};

export const editedTodo = (id: string, text: string): EditedTodoAction => ({
  type: EDITED_TODO,
  id,
  text
});

export const editTodo = (id: string, text: string): ThunkAction<TodosAction> => (dispatch) => {
  dispatch({ type: EDIT_TODO, id, text });

  editTodoRequest(id, text).then(() => {
    SocketIOHelper.getSocket().emit(SocketActions.EDIT_TODO, id, text);
  }).catch((error) => {
    dispatch({ type: GET_TODOS });
    console.log(`error: edit todo -> ${error}`);
  });
};

export const VisibilityFilters = {
  SHOW_ALL: "SHOW_ALL",
  SHOW_COMPLETED: "SHOW_COMPLETED",
  SHOW_ACTIVE: "SHOW_ACTIVE"
};
