// @flow

export const ADD_TODO = "ADD_TODO";
export const TODO_ADDED = "TODO_ADDED";
export const GET_TODOS = "GET_TODOS";
export const RECEIVE_TODOS = "RECEIVE_TODOS";
export const TOGGLE_TODO = "TOGGLE_TODO";
export const TOGGLED_TODO = "TOGGLED_TODO";
export const MOVE_TODO = "MOVE_TODO";
export const MOVED_TODO = "MOVED_TODO";
export const REMOVE_TODO = "REMOVE_TODO";
export const REMOVED_TODO = "REMOVED_TODO";
export const EDIT_TODO = "EDIT_TODO";
export const EDITED_TODO = "EDITED_TODO";
export const SET_VISIBILITY_FILTER = "SET_VISIBILITY_FILTER";
