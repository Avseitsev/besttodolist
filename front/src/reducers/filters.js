// @flow
import { SET_VISIBILITY_FILTER } from "./actions/actions";
import { VisibilityFilters } from "./actions";

interface FiltersState {
  visibilityFilter: string;
}

export interface SetVisibilityFilterAction { type: typeof SET_VISIBILITY_FILTER, filter: string }

type visibilityFilterAction = SetVisibilityFilterAction;

const filters = (state: FiltersState, action: visibilityFilterAction): FiltersState => {
  switch (action.type) {
    case SET_VISIBILITY_FILTER:
      return { ...state, visibilityFilter: action.filter };
    default:
      return state || { visibilityFilter: VisibilityFilters.SHOW_ALL };
  }
};

export default filters;
