// @flow
import filters from "../filters";
import {
  SET_VISIBILITY_FILTER
} from "../actions/actions";

describe("Test visibilityFilterReducers", () => {

  it("Reducer for SET_VISIBILITY_FILTER", () => {
    let state = {
      visibilityFilter: "ALL"
    };

    state = filters(state, {
      type: SET_VISIBILITY_FILTER,
      filter: "SHOW_COMPLETED"
    });

    expect(state).toEqual({
      visibilityFilter: "SHOW_COMPLETED"
    });
  });
});