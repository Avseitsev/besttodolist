// @flow
import React, { PureComponent } from "react";
import { FormattedMessage } from "react-intl";
import { connect } from "react-redux";
import Loader from "react-loader-spinner";
import { PageWrapper, Header, FormWrapper } from "./Main.style";
import Footer from "../../components/footer/Footer";
import AddTodo from "../../containers/AddTodo";
import VisibleTodoList from "../../containers/VisibleTodoList";
import { getTodos as getTodosAction } from "../../reducers/actions";

type Props = {
  getTodos: Function,
  isLoading: boolean
}

class Main extends PureComponent<Props> {

  componentDidMount() {
    const { getTodos } = this.props;

    getTodos();
  }

  render() {
    const { isLoading } = this.props;

    return (
      <PageWrapper className="page">
        <Header>
          <FormattedMessage id="header.title.toDoList" />
        </Header>
        {isLoading ?
          <FormWrapper>
            <Loader
              type="Puff"
              color="black"
            />
          </FormWrapper>
          :
          <FormWrapper>
            <Footer />
            <AddTodo />
            <VisibleTodoList />
          </FormWrapper>
        }
      </PageWrapper>
    );
  }
}

const mapStateToProps = state => ({
  isLoading: state.todos.isLoading
});

const mapDispatchToProps = dispatch => ({
  getTodos: () => dispatch(getTodosAction())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Main);
