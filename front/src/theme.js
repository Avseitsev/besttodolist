const inputHeigth = 30;
const buttonMargin = 10;

const theme = {

  page: {
    padding: 50,
    bacground: {
      color: "darkgray"
    },
    header: {
      height: 50,
      bacground: {
        color: "#1fc8db"
      },
    },
    formWrapper: {
      minHeight: 100,
      width: 400,
      padding: 40,
      marginTop: 50,
      bacground: {
        color: "#1fc8db"
      },
    },
    footer: {
      padding: {
        bottom: 10
      }
    }
  },

  footer: {
    button: {
      margin: buttonMargin
    }
  },

  addTodoForm: {
    button: {
      margin: buttonMargin
    }
  },

  input: {
    height: inputHeigth,
    border: {
      radius: 5
    }
  },

  todo: {
    padding: 5
  },

  button: {
    height: inputHeigth,
    minWidth: 100,
    color: "#fff",
    bacground: {
      activeColor: "#273439",
      disabledColor: "dimgrey",
    },
    border: {
      radius: 10,
      activeColor: "#20911e"
    },
    boxShadow: {
      color: "#356b0b"
    }
  }
};

export default theme;