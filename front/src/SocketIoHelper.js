// @flow
import SocketIOClient from "socket.io-client";
import { basePath } from "./settings";

export const SocketActions = {
  ADD_TODO: "add todo",
  TOGGLE_TODO: "toggle todo",
  MOVE_TODO: "move todo",
  REMOVE_TODO: "remove todo",
  EDIT_TODO: "edit todo"
};

const socket = new SocketIOClient(basePath);

class SocketIOHelper {
  getSocket = () => {
    return socket;
  }
}

export default new SocketIOHelper();
