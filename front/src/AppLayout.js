/* eslint-disable camelcase */
import React from "react";
import { IntlProvider, addLocaleData } from "react-intl";
import { ThemeProvider } from "styled-components";
import locale_en from "react-intl/locale-data/en";
import Main from "./pages/main/Main";
import messagesEN from "./translations/en.json";
import theme from "./theme";

addLocaleData([...locale_en]);

const AppLayout = () => (
  <div className="App">
    <ThemeProvider theme={theme}>
      <IntlProvider locale="en" messages={messagesEN}>
        <Main />
      </IntlProvider>
    </ThemeProvider>
  </div>
);

export default AppLayout;
