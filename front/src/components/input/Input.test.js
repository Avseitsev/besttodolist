// @flow
import React from "react";
import renderer from "react-test-renderer";
import Enzyme from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import { ThemeProvider } from "styled-components";
import theme from "../../theme";
import Input from "./Input";
import "jest-styled-components";

Enzyme.configure({ adapter: new Adapter() });

describe("Input component:", () => {

  it("Input renders correctly", () => {

    const btn = renderer.create(
      <ThemeProvider theme={theme} >
        <Input onChange={jest.fn()} value="value" placeholder="placeholder" />
      </ThemeProvider>
    );
    const tree = btn.toJSON();

    expect(tree).toMatchSnapshot();
  });

  it("Should call onChange prop", () => {
    const onChange = jest.fn();
    const event = {
      preventDefault() {},
      target: { value: "the-value" }
    };
    const component = Enzyme.mount(
      <ThemeProvider theme={theme} >
        <Input onChange={onChange} value="value" placeholder="placeholder" />
      </ThemeProvider>
    );

    component.find("input").simulate("change", event);
    expect(onChange).toBeCalledWith("the-value");
  });
});
