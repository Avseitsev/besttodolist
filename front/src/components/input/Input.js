// @flow
import React, { Component } from "react";
import { InputWrapper } from "./Input.style";

type Props = {
  onChange: Function,
  value: string,
  placeholder: string
};

export default class Input extends Component<Props, {}> {

  onChange = (event: Object): void => {
    const { onChange } = this.props;

    onChange(event.target.value);
  }

  render() {
    const { placeholder, value } = this.props;

    return (
      <InputWrapper className="input">
        <input onChange={this.onChange} value={value} placeholder={placeholder} />
      </InputWrapper>
    );
  }
}
