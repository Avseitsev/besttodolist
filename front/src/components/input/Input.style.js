import styled from "styled-components";

export const InputWrapper = styled.div`
  width: 100%;
  
  input {
    width: 100%;
    height: ${p => p.theme.input.height}px;
    border-radius: ${p => p.theme.input.border.radius}px;
    font-weight: bold;
    outline: none;
    font-size: 1rem;

    &:hover {
      opacity:.85;
    }

    &:focus {
      opacity:.85;
    }
  }
`;