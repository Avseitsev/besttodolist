// @flow
import React from "react";
import Enzyme from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import renderer from "react-test-renderer";
import { ThemeProvider } from "styled-components";
import theme from "../../theme";
import Button from "./Button";
import "jest-styled-components";

Enzyme.configure({ adapter: new Adapter() });

describe("Button component:", () => {
  const label = "label";

  it("Button renders correctly", () => {
    const btn = renderer.create(
      <ThemeProvider theme={theme} >
        <Button onClick={jest.fn()} label={label} />
      </ThemeProvider>
    );
    const tree = btn.toJSON();

    expect(tree).toMatchSnapshot();
  });

  it("Desabled Button renders correctly", () => {
    const btn = renderer.create(
      <ThemeProvider theme={theme} >
        <Button onClick={jest.fn()} label={label} disabled />
      </ThemeProvider>
    );
    const tree = btn.toJSON();

    expect(tree).toMatchSnapshot();
  });

  it("Should handle the click event", () => {
    const fn = jest.fn();
    const btn = Enzyme.mount(
      <ThemeProvider theme={theme} >
        <Button onClick={fn} label={label} />
      </ThemeProvider>
    );

    btn.find("button").simulate("click");
    expect(fn).toHaveBeenCalledWith();
  });

  it("Should not handle the click event", () => {
    const fn = jest.fn();
    const btn = Enzyme.mount(
      <ThemeProvider theme={theme} >
        <Button onClick={fn} label={label} disabled/>
      </ThemeProvider>
    );

    btn.find("button").simulate("click");
    expect(fn).not.toHaveBeenCalled();
  });

});
