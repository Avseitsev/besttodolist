// @flow
import React, { PureComponent } from "react";
import { ButtonWrapper } from "./Button.style";

type Props = {
  onClick: Function,
  label: string,
  disabled?: boolean
};

export default class Button extends PureComponent<Props> {
  static defaultProps = {
    disabled: false
 };

  onClick = (): void => {
    const { onClick, disabled } = this.props;

    if (!disabled) {
      onClick();
    }
  }

  render() {
    const { label, disabled } = this.props;

    return (
      <ButtonWrapper disabled={disabled}>
        <button onClick={this.onClick} disabled={disabled} type="button">{label}</button>
      </ButtonWrapper>
    );
  }
}
