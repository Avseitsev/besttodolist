// @flow
import React, { Component } from "react";
import { injectIntl } from "react-intl";
import Input from "../input/Input";
import Button from "../button/Button";
import { FormWrapper } from "./AddTodoForm.style";

type Props = {
  onClick: Function,
  intl: Object
};

type State = {
  inputValue: string
};

class AddTodoForm extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      inputValue: ""
    };
  };

  onChange = (value: string): void => {
    this.setState({
      inputValue: value
    });
  }

  onClick = (): void => {
    const { onClick } = this.props;
    const { inputValue } = this.state;

    if (inputValue) {
      onClick(inputValue);
      this.setState({
        inputValue: ""
      });
    }
  }

  render() {
    const { inputValue } = this.state;
    const { intl } = this.props;

    return (
      <FormWrapper className="AddTodoForm">
        <Input onChange={this.onChange} value={inputValue} placeholder={intl.formatMessage({ id: "addTodoForm.input.placeholder" })} />
        <Button onClick={this.onClick} label={intl.formatMessage({ id: "addTodoForm.button.addTodo" })} />
      </FormWrapper>
    );
  }
}

export default injectIntl(AddTodoForm);
