import styled from "styled-components";

export const FormWrapper = styled.div`
  display: flex;

  button {
    margin-left: ${p => p.theme.addTodoForm.button.margin}px;
  }
`;