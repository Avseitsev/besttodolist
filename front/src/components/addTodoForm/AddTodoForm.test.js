// @flow
import React from "react";
import Enzyme from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import { ThemeProvider } from "styled-components";
import { IntlProvider } from "react-intl";
import messagesEN from "../../translations/en.json";
import theme from "../../theme";
import AddTodoForm from "./AddTodoForm";
import "jest-styled-components";

Enzyme.configure({ adapter: new Adapter() });

describe("AddTodoForm component:", () => {
  const locale = "en";

  it("AddTodoForm renders correctly", () => {
    const btn = Enzyme.shallow(
      <IntlProvider locale={locale} messages={messagesEN}>
        <ThemeProvider theme={theme} >
          <AddTodoForm onClick={jest.fn()} />
        </ThemeProvider>
      </IntlProvider>
    );

    expect(btn).toMatchSnapshot();
  });
});
