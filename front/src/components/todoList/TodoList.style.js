import styled from "styled-components";

export const TodoListWrapper = styled.div`

  ul {
    padding-left: unset;
  }

  li {
    list-style-type: none;
    width: fit-content;
  }
`;
