// @flow
import React from "react";
import renderer from "react-test-renderer";
import Enzyme from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import { ThemeProvider } from "styled-components";
import theme from "../../../theme";
import Todo, { modes } from "./Todo";
import "jest-styled-components";

Enzyme.configure({ adapter: new Adapter() });

describe("Todo component:", () => {
  const todo = {
    id: "testId",
    completed: true,
    text: "text"
  };

  it("Completed Todo renders correctly", () => {
    const btn = renderer.create(
      <ThemeProvider theme={theme} >
        <Todo
          onClick={jest.fn()}
          todo={todo} />
      </ThemeProvider>
    );
    const tree = btn.toJSON();

    expect(tree).toMatchSnapshot();
  });

  it("Unfinished Todo renders correctly", () => {
    const btn = renderer.create(
      <ThemeProvider theme={theme} >
        <Todo
          onClick={jest.fn()}
          todo={todo} />
      </ThemeProvider>
    );
    const tree = btn.toJSON();

    expect(tree).toMatchSnapshot();
  });

  it("Should handle the click event on span", () => {
    const fn = jest.fn();
    const btn = Enzyme.mount(
      <ThemeProvider theme={theme} >
        <Todo
          onClick={fn}
          todo={todo} />
      </ThemeProvider>
    );

    btn.find("span").simulate("click");
    expect(fn).toBeCalledWith(todo.id);
  });

  it("Save cycle this mode change", () => {
    const onSave = jest.fn();
    const event = {
      preventDefault() {},
      target: { value: "the-value" }
    };
    const btn = Enzyme.mount(
      <ThemeProvider theme={theme} >
        <Todo
          onClick={jest.fn()}
          onSave={onSave}
          todo={todo} />
      </ThemeProvider>
    );

    expect(btn.find(Todo).state().mode).toEqual(modes.view);

    btn.find("svg").simulate("click");
    expect(btn.find(Todo).state().mode).toEqual(modes.edit);

    btn.find("input").simulate("change", event);
    expect(btn.find(Todo).state().newText).toEqual("the-value");

    btn.find("svg").simulate("click");
    expect(onSave).toBeCalledWith(todo.id, "the-value");
    expect(btn.find(Todo).state().mode).toEqual(modes.view);
  });
});
