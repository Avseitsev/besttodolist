// @flow
import React, { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit, faSave } from "@fortawesome/free-solid-svg-icons";
import { TexWrapper, TodoWrapper } from "./Todo.style";
import Input from "../../input/Input";
import type { TodoObject } from "../TodoList";

export const modes = {
  view: "view",
  edit: "edit"
};

type Props = {
  onClick: Function,
  onSave: Function, 
  todo: TodoObject,
};

type State = {
  mode: string,
  newText: string,
};

export default class Todo extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      mode: modes.view,
      newText: props.todo.text
    };
  };

  modeChange = (mode: string): void =>{
    const { todo } = this.props;

    this.setState({
      mode,
      newText: todo.text
    });
  }

  onTextClick = (): void => {
    const { onClick, todo } = this.props;

    onClick(todo.id);
  }

  onSave = (): void => {
    const { onSave, todo } = this.props;
    const { newText } = this.state;

    if (newText !== todo.text) {
      onSave(todo.id, newText);
    }

    this.modeChange(modes.view);
  }

  onChange = (value: string): void => {
    this.setState({
      newText: value
    });
  }

  renderView = () => {
    const { todo } = this.props;

    return (
      <TodoWrapper>
        <FontAwesomeIcon icon={faEdit} size="xs" onClick={() => this.modeChange(modes.edit)}/>
        <TexWrapper onClick={this.onTextClick} completed={todo.completed}>
          {todo.text}
        </TexWrapper>
      </TodoWrapper>
    );
  }

  renderEdit = () => {
    const { todo } = this.props;
    const { newText } = this.state;

    return (
      <TodoWrapper>
        <FontAwesomeIcon icon={faSave} size="xs" onClick={this.onSave}/>
        <Input value={newText} onChange={this.onChange} placeholder={todo.text}/>
      </TodoWrapper>
    );
  }

  render() {
    const { mode } = this.state;

    switch (mode) {
      case modes.view:    
        return this.renderView();
      case modes.edit:
        return this.renderEdit();
      default:
        throw new Error(`Unknown mode: ${mode}`);
    }
  }
}

