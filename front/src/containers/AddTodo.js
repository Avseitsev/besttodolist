// @flow
import { connect } from "react-redux";
import { addTodo } from "../reducers/actions";
import AddTodoForm from "../components/addTodoForm/AddTodoForm";

const mapStateToProps = () => ({});

const mapDispatchToProps = ( dispatch ) => ({
  onClick: (todoText: string) => dispatch(addTodo(todoText))
});

export default connect(mapStateToProps, mapDispatchToProps)(AddTodoForm);

